
// Playing Cards
// Spencer Sauberlich

#include <iostream>
#include <conio.h>

using namespace std;




enum Rank
{ 
	Two = 2,
	Three, 
	Four, 
	Five, 
	Six, 
	Seven, 
	Eight, 
	Nine, 
	Ten, 
	Jack, 
	Queen, 
	King, 
	Ace
};
enum Suit
{
	Spade,
	Club,
	Heart,
	Diamond
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) return card1;
	return card2;
}

int main()
{
	Card c;
	c.rank = Eight;
	c.suit = Diamond;
	Card c2;
	c2.rank = Ace;
	c2.suit = Spade;

	PrintCard(HighCard(c, c2));


	_getch();
	return 0;
}

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
	case Two: cout << "two "; break; 
	case Three: cout << "three "; break; 
	case Four: cout << "four "; break; 
	case Five: cout << "Five "; break; 
	case Six: cout << "Six "; break;
	case Seven: cout << "Seven "; break; 
	case Eight: cout << "Eight "; break; 
	case Nine: cout << "Nine "; break; 
	case Ten: cout << "Ten "; break; 
	case Jack: cout << "Jack "; break; 
	case Queen: cout << "Queen "; break; 
	case King: cout << "King "; break; 
	case Ace: cout << "Ace "; break; 
	}
	cout << "of ";
	switch (card.suit)
	{
	case Spade: cout << "Spades\n"; break;
	case Club: cout << "Clubs\n"; break;
	case Heart: cout << "Hearts "; break;
	case Diamond: cout << "Diamonds "; break;
	}


}

